<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{

    public function index()
    {
        $mahasiswa = Mahasiswa::latest()->paginate(5);

        $title = 'Hapus Data!';
        $text = "Yakin akan hapus data?";
        confirmDelete($title, $text);

        return view('welcome',[
            "data" => $mahasiswa
        ]);
    }

    public function create(){
        return view('tambah');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'nim' => 'required|unique:mahasiswas',
            'nama' => 'required',
            'prodi' => 'required',
            'fakultas' => 'required',
            'jenis_kelamin' => 'required'
        ]);

        // Mengonversi semua input menjadi huruf kecil
        $validated['nama'] = strtolower($validated['nama']);
        $validated['prodi'] = strtolower($validated['prodi']);
        $validated['fakultas'] = strtolower($validated['fakultas']);

        Mahasiswa::create($validated);
        return redirect('/mahasiswa')->with('success','Data <strong>' . ucwords($validated['nama']) . '</strong> berhasil ditambahkan');
    }

    public function edit($id){

        $mahasiswa = Mahasiswa::find($id);
        return view('edit',[
            'data' => $mahasiswa
        ]);
    }

    public function update($id, Request $request){


        $mahasiswa = Mahasiswa::find($id);

        $validated = $request->validate([
            'nim' => 'required',
            'nama' => 'required',
            'prodi' => 'required',
            'fakultas' => 'required',
            'jenis_kelamin' => 'required'
        ]);

        // Mengonversi semua input menjadi huruf kecil
        $validated['nama'] = strtolower($validated['nama']);
        $validated['prodi'] = strtolower($validated['prodi']);
        $validated['fakultas'] = strtolower($validated['fakultas']);

        $mahasiswa->update($validated);
        return redirect('/mahasiswa')->with('success','Data <strong>' . ucwords($validated['nama']) . '</strong> berhasil diedit');
    }


    public function destroy($id){
        $mahasiswa = Mahasiswa::find($id);
        $mahasiswa->delete();
        return redirect('/mahasiswa')->with('success','Data <strong>' . ucwords($mahasiswa['nama']) . '</strong> berhasil dihapus');
    }
}
