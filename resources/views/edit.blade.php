@extends('layouts.main')

@section('content')
    <h1>Form Tambah</h1>
    <form method="post" action="/mahasiswa/{{ $data->id }}">
        @method('put')
        @csrf
        <div class="mb-3">
            <label for="nim" class="form-label">NIM</label>
            <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim" name="nim" value="{{ $data->nim }}">
            @error('nim')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ $data->nama }}">
            @error('nama')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="prodi" class="form-label">prodi</label>
            <input type="text" class="form-control @error('prodi') is-invalid @enderror" id="prodi" name="prodi" value="{{ $data->prodi }}">
            @error('prodi')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="fakultas" class="form-label">fakultas</label>
            <input type="text" class="form-control @error('fakultas') is-invalid @enderror" id="fakultas" name="fakultas" value="{{ $data->fakultas }}">
            @error('fakultas')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">Jenis Kelamin</label><br>
            <input type="radio" id="laki-laki" name="jenis_kelamin" value="laki-laki" {{ $data->jenis_kelamin == 'laki-laki' ? 'checked' : '' }}>
            <label for="laki-laki" class="me-4">Laki-laki</label>
            <input type="radio" id="perempuan" name="jenis_kelamin" value="perempuan" {{ $data->jenis_kelamin == 'perempuan' ? 'checked' : '' }}>
            <label for="perempuan">Perempuan</label><br>

        </div>

        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>

@endsection
