@extends('layouts.main')

@section('content')
    <h1>DATA MAHASISWA</h1>
    <a href="/mahasiswa/create" class="btn btn-primary mt-5 mb-3">Tambah Data</a>
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {!! session('success') !!}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <table class="table table-striped" id="myTable">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">NIM</th>
                <th scope="col">NAMA</th>
                <th scope="col">PRODI</th>
                <th scope="col">FAKULTAS</th>
                <th scope="col">JENIS KELAMIN</th>
                <th scope="col">ACTION</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $no => $d)

                <tr>
                    <td scope="col" class="text-capitalize">{{ $data->firstItem() + $no }}</td>
                    <td scope="col" class="text-capitalize">{{ $d->nim }}</td>
                    <td scope="col" class="text-capitalize">{{ $d->nama }}</td>
                    <td scope="col" class="text-capitalize">{{ $d->prodi }}</td>
                    <td scope="col" class="text-capitalize">{{ $d->fakultas }}</td>
                    <td scope="col" class="text-capitalize">{{ $d->jenis_kelamin }}</td>
                    <td scope="col" class="text-capitalize">
                        <a href="/mahasiswa/{{ $d->id }}/edit" class="btn btn-warning">Edit</a>
                        <a href="/mahasiswa/{{ $d->id }}" class="btn btn-danger" data-confirm-delete="true">Delete</a>
                        {{-- <form action="/mahasiswa/{{ $d->id }}" method="post" class="d-inline">
                            @csrf
                            @method("delete")
                            <button type="submit" class="btn btn-danger" data-confirm-delete="true" >Delete</button>
                            onclick="return confirm('Hapus {{ $d->nama }}?')"
                        </form> --}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-between mt-4">
        <div class="">
            <p>Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of {{ $data->total() }}</p>
        </div>
        <div class="">
            {{ $data->links() }}
        </div>
    </div>

@endsection

