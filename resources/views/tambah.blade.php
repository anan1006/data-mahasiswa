@extends('layouts.main')

@section('content')
    <h1>Form Tambah</h1>
    <form method="post" action="/mahasiswa">
        @csrf
        <div class="mb-3">
            <label for="nim" class="form-label">NIM</label>
            <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim" name="nim" value="{{ old("nim") }}">
            @error('nim')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old("nama") }}">
            @error('nama')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="prodi" class="form-label">prodi</label>
            <input type="text" class="form-control @error('prodi') is-invalid @enderror" id="prodi" name="prodi" value="{{ old("prodi") }}">
            @error('prodi')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="fakultas" class="form-label">fakultas</label>
            <input type="text" class="form-control @error('fakultas') is-invalid @enderror" id="fakultas" name="fakultas" value="{{ old("fakultas") }}">
            @error('fakultas')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">Jenis Kelamin</label><br>
            <input type="radio" id="laki-laki" name="jenis_kelamin" value="laki-laki">
            <label for="laki-laki" class="me-4">Laki-laki</label>
            <input type="radio" id="perempuan" name="jenis_kelamin" value="perempuan">
            <label for="perempuan">Perempuan</label><br>

        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>


@endsection
