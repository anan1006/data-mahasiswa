<?php

use App\Http\Controllers\MahasiswaController;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// Route::get('/mahasiswa/create', [MahasiswaController::class, 'create']);
// Route::post('/mahasiswa', [MahasiswaController::class, 'store']);
// Route::get('/mahasiswa', [MahasiswaController::class, 'index']);
// Route::get('/mahasiswa/{id}/edit', [MahasiswaController::class, 'edit']);
// Route::put('/mahasiswa/{id}', [MahasiswaController::class, 'update']);
// Route::delete('/mahasiswa/{id}', [MahasiswaController::class, 'destroy']);

Route::resource('mahasiswa', MahasiswaController::class);

